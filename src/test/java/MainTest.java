import org.assertj.core.data.Offset;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class MainTest {

    public final static double EPS = Main.EPS; //0.000010000

    Main main;

    @BeforeEach
    void setUp() {
        main = new Main();
    }

    // Пункт 3
    @ParameterizedTest
    @CsvSource({"0.7, 1","1, 0","1.2, -0.13127", "2, -0.24046"})
    void testFun(double x, double expectedY) {
        double y = Main.fun(x);
        //System.out.println("x = " + x + ", result is: " + y);
        //assertEquals(expectedY, y, EPS);
        assertThat(y).isCloseTo(expectedY, Offset.offset(EPS));
    }

    // Пункт 4
    @Test
    void testGetStepCount() {
        assertEquals(750, main.getStepCount());
    }

    // Пункт 5
    @ParameterizedTest
    @CsvSource({"175, 0.7000","350, 1.4000","750, 3.0000"})
    void testGetXbyElement(int element, double expectedValue){
        assertEquals(expectedValue, main.getXbyElement(element), EPS);
    }

    @ParameterizedTest
    @CsvSource({"175, 1","350, -0.32974","750, 0.21424"})
    void testGetYbyElement(int element, double expectedValue){
        double y = main.getYbyElement(element);
        //System.out.println("x = " + main.getXbyElement(element) + ", y = " + y);
        assertEquals(expectedValue, y, EPS);
    }

    // Пункт 6

    @Test
    void testGetMinYElement() {
        assertEquals(362, main.getMinYElement());
    }

    @Test
    void testGetMaxYElement() {
        assertEquals(0, main.getMaxYElement());
    }

    @Test
    void testGetMinYValue() {
        assertEquals(-0.47026, main.getYbyElement(main.getMinYElement()), EPS);
    }

    @Test
    void testGetMaxYValue() {
        assertEquals(1, main.getYbyElement(main.getMaxYElement()), EPS);
    }

    @Test
    void testGetSumYElements() {
        assertEquals(118.94365, main.getSumYElements(), EPS);
    }

    @Test
    void testCalcArithmeticMean() {
        assertEquals(0.15838, main.calcArithmeticMean(), EPS);
    }
}
