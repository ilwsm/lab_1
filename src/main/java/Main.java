public class Main {
    public final static double EPS = 1e-5; //0.000010000

    final double first = 0.0;
    final double last = 3.0;
    final double step = 0.004;
    private int arraySize;
    private double[] yArr;
    private double[] xArr;

    public Main() {
        calcElementCount();
        createArrays();
    }

    public static void main(String[] args) {
        new Main().go();
    }

    public static double fun(double x) {
        final double a = -0.5;
        final double b = 2;

        if (x <= 0.7 + EPS) {
            return 1;
        } else if (x <= 1.4 + EPS) {
            return a * Math.pow(x, 2) * Math.log(x);
        } else {
            return Math.exp(a * x) * Math.cos(b * x);
        }
    }

    private void go() {

        System.out.println("Step count: " + getStepCount());
        System.out.println("Sum of all y values: " + getSumYElements());
        System.out.println("Arithmetic Mean of y values: " + calcArithmeticMean());

        printMax();
        printMin();
    }

    private void calcElementCount() {
        arraySize = (int) ((last - first) / step) + 1;
    }

    public int getStepCount() {
        // Тут не совсем понял, предположу что в диапазоне от 0 до 3 - шагов 750, а x элементов 751
        return arraySize - 1;
    }

    private void createArrays() {
        yArr = new double[arraySize];
        xArr = new double[arraySize];
        double x = first;
        for (int i = 0; i < arraySize; i++) {
            double y = fun(x);
            yArr[i] = y;
            xArr[i] = x;
            x += step;
        }
    }

    public double getXbyElement(int element) {
        return xArr[element];
    }

    public double getYbyElement(int element) {
        return yArr[element];
    }

    public int getMinYElement() {
        double min = yArr[0];
        int index = 0;
        for (int i = 1; i < yArr.length; i++) {
            if (yArr[i] < min) {
                min = yArr[i];
                index = i;
            }
        }

        return index;
    }

    public int getMaxYElement() {
        double min = yArr[0];
        int index = 0;
        for (int i = 1; i < yArr.length; i++) {
            if (yArr[i] > min) {
                min = yArr[i];
                index = i;
            }
        }

        return index;
    }

    public double getSumYElements() {
        double sum = 0;
        for (double v : yArr) {
            sum += v;
        }
        return sum;
    }

    public double calcArithmeticMean() {
        return getSumYElements() / yArr.length;
    }

    public void printMax() {
        int max = getMaxYElement();
        double x = getXbyElement(max);
        double y = getYbyElement(max);

        System.out.println("Max y value: " + y + ", x = " + x + ". Element " + max);
    }

    public void printMin() {
        int min = getMinYElement();
        double x = getXbyElement(min);
        double y = getYbyElement(min);

        System.out.println("Min y value: " + y + ", x = " + x + ". Element " + min);
    }
}
